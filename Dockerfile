#Linux image
FROM alpine
WORKDIR /root/helloWorld
COPY helloWorld.java /root/helloWorld 

#install JDK
RUN apk add openjdk8
ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ENV PATH $PATH:$JAVA_HOME/bin

#Compile our HelloWorld
RUN javac helloWorld.java

ENTRYPOINT java helloWorld